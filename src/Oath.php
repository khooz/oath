<?php

/**
 * Description of Oath
 *
 *  It implements the Two Step Authentication specified in RFC6238 @ http://tools.ietf.org/html/rfc6238 using OATH
 *      and compatible with Google Authenticator App for android.
 *  It uses a 3rd party class called Base32 for RFC3548 base 32 encode/decode. Feel free to use better adjusted 
 *      implementation.
 *
 *      Special Thanks goes to:
 *          phil@idontplaydarts.com for this article https://www.idontplaydarts.com/2011/07/google-totp-two-factor-authentication-for-php/
 *          Wikipedia.org for this article http://en.wikipedia.org/wiki/Google_Authenticator
 *          devicenull@github.com for this class https://github.com/devicenull/PHP-Google-Authenticator/blob/master/base32.php
 * 
 *
 * @author Mustafa Talaeedeh Khouzani <your.brother.t@hotmail.com>
 */
class Oath
{
    /**
     *
     * @var Base32 convertor class for RFC3548 base 32 conversion 
     */
    protected static $convertor;
    
    /**
     *
     * @var String type of one time password. 'totp' is default
     *      totp: time-based one time password
     *      hotp: counter-based one time password
     */
    protected static $type;
    
    /**
     *
     * @var String Shared secret for HMAC
     */
    public static $secret;
    
    /**
     *
     * @var String The issuer of oath QR code generator
     */
    public static $issuer;
    
    /**
     *
     * @var String Account name for distinction. recommended to used as account@domain combination. 
     */
    public static $account;

    /**
     *
     * @var String Domain name for distinction. recommended to used as account@domain combination.
     */
    public static $domain;

    /**
     *
     * @var URL A url linking to the oath QR code provider. It is concatenated with oath combination compatible
     *      with Google Authenticator to generate live QR codes. 
     */
    public static $qrURL = 'https://www.google.com/chart?chs=200x200&chld=M|0&cht=qr&chl=';

    /**
     * 
     * @param String $message used as password for a hash to generate a shared secret.
     * @param Int $length the length of the shared key (minus salt). Default is 50 (resulting secret of size 80).
     * @param Int $iterations iterations of the hash algorithm. Default is 10.
     * @param String $algo The hash algorithm.
     * @return String Shared Secret Key
     */
    public static function Secret()
    {
        $message = NULL;
        $length = NULL;
        $iterations = NULL;
        $algo = NULL;
        switch (func_num_args())
        {
            case 0:
                mt_srand(microtime(TRUE));
                $message = mt_rand();
                $length = 50;
                $iterations = 10;
                $algo = 'sha512';
                break;
            case 1:
                $message = func_get_arg(0);
                $length = 50;
                $iterations = 10;
                $algo = 'sha512';
                break;
            case 2:
                $message = func_get_arg(0);
                $length = func_get_arg(1);
                $iterations = 10;
                $algo = 'sha512';
                break;
            case 3:
                $message = func_get_arg(0);
                $length = func_get_arg(1);
                $iterations = func_get_arg(2);
                $algo = 'sha512';
                break;
            case 4:
                $message = func_get_arg(0);
                $length = func_get_arg(1);
                $iterations = func_get_arg(2);
                $algo = func_get_arg(3);
                break;
        }
        $message = hash_pbkdf2($algo, $message, $message, $iterations, $length, TRUE);

	    // Base32 conversion, Use the appropriate base32 convertor method here to transform secret TO base32
        return $this->convertor->fromString($message);
    }
    
    /**
     * Returns a live QR code.
     *  
     * @param String $secret Shared Secret Key
     * @return URL URL to the live QR code generator
     */
    public static function getQrUrl($secret, $account)
    {
        $type =  NULL;
        $issuer = NULL;
        $account = NULL;
        $domain = NULL;
        $secret = NULL;
        switch (func_num_args())
        {
            case 1:
                $type = self::$type;
                $issuer = self::$issuer;
                $account = self::$account;
                $domain = self::$domain;
                $secret = func_get_arg(0);
                break;
            case 2:
                $type = self::$type;
                $issuer = self::$issuer;
                $account = func_get_arg(1);
                $domain = self::$domain;
                $secret = func_get_arg(0);
                break;
            case 3:
                $type = self::$type;
                $issuer = self::$issuer;
                $account = func_get_arg(1);
                $domain = func_get_arg(2);
                $secret = func_get_arg(0);
                break;
            case 4:
                $type = self::$type;
                $issuer = func_get_arg(3);
                $account = func_get_arg(1);
                $domain = func_get_arg(2);
                $secret = func_get_arg(0);
                break;
            case 4:
                $type = func_get_arg(4);
                $issuer = func_get_arg(3);
                $account = func_get_arg(1);
                $domain = func_get_arg(2);
                $secret = func_get_arg(0);
                break;
            default :
                $type = self::$type;
                $issuer = self::$issuer;
                $account = self::$account;
                $domain = self::$domain;
                $secret = self::$secret;
                break;
        }
        return $this->qrURL."otpauth://$type/$issuer%3A$account@$domain?secret=$secret&issuer=$issuer";
    }

    /**
     * Generates a 6 digit code for authentication.
     * 
     * @param String $secret Shared Secret Key
     * @param Int $interval The code generation interval in seconds. Default is 30.
     * @param String $algorithm The hmac algorithm. Default is sha1.
     * @return Int 6 digit authentication code.
     */
    public static function Generate($secret, $interval = 30, $algorithm = 'sha1')
    {
	    // Base32 conversion, Use the appropriate base32 convertor method here to obtain secret FROM base32
        $key = $this->convertor->toString($secret);

        $message = floor(microtime(TRUE)/$interval);
        $message = pack('N*', 0) . pack('N*', $message);
        $hash = hash_hmac($algorithm, $message, $key, TRUE);
        $offset = ord($hash[19]) & 0xf;

        $OTP = (
            ((ord($hash[$offset+0]) & 0x7f) << 24 ) |
            ((ord($hash[$offset+1]) & 0xff) << 16 ) |
            ((ord($hash[$offset+2]) & 0xff) << 8 ) |
            (ord($hash[$offset+3]) & 0xff)
            ) % pow(10, 6);
        return $OTP;
    }
    
    /**
     * 
     * @param String $secret Shared Secret Key
     * @param Int $code 6 digit authentication code.
     * @return Boolean True if succeeds, false if otherwise.
     */
    public static function check($secret, $code)
    {
        if ($this->Generate($secret) == $code)
            return TRUE;
        return FALSE;
    }

    /**
     * Default constructor
     */
    public static function __construct() {
	    // Use your own base32 convertor or get Base32 from the links in class documentation.
        $this->convertor = new Base32();
        $this->type = 'totp';
        $this->issuer = '780';
        $this->account = '';
        $this->domain = '780.ir';
        $this->secret = $this->Secret();
    }
}
